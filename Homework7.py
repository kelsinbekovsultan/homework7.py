# Напишите приммеры использования всех операций со словарями
    # copy()
dic = {
        'a': 'b',
        'b': 'c',
    }
dic.copy()
print(dic)

    # fromKeys()
name = {'a', 'b', 'c', 'd', 'e'}
value = 'Bazar jok'
print(dict.fromkeys(name, value))
    # get()
print(dic.get('a'))
    # items()
print(dic.items())
for name, value in dic.items():
    print(name, value)
    # keys()
print(dic.keys())
    # values()
print(dic.values())
    # pop()
print(dic.pop('b'))
    # popitem()
print(dic.popitem())
    # clear()
dic.clear()
print(dic)
# Оберните все операции в функции, которые принимают словарь и выполняют над ним операцию. Функцию надо вызвать.
dict1 = {'aaa', 'bbb', 'ccc', 'ddd', 'eee'}
    # copy()
def oper(dict):
    print(dict.copy())
oper(dict1)
    # fromkeys()
key = {'bcd', 'dfr', 'lkj'}
val = 'men'
def oper1(key, val):
    print(dict.fromkeys(key, val))

oper1(key, val)
    # get()
dic1 = {
        'a': 'b',
        'b': 'c',
        'c': 'd'
    }
def oper2(dic1):
    print(dic1.get('a'))
oper2(dic1)
    # items()
def oper3(dic1):
    print(dic1.items())
oper3(dic1)
    # keys()
def oper4(dic1):
    print(dic1.keys())
oper4(dic1)
    # values()
def oper5(dic1):
    print(dic1.values())
oper5(dic1)
    # pop()
def oper6(dic1):
    print(dic1.pop('b'))
oper6(dic1)
    # popitem()
def oper7(dic1):
    print(dic1.popitem())
oper7(dic1)
    # clear()
def oper8(dic1):
    dic1.clear()
    print(dic1)
oper8(dic1)
# Задача для гугления и самостоятельной работы. Разобраться как работает метод dict.update() и dict.setdefault()
# dict.update()
s1 = {
    'name': 'Toli',
    'age': 17
     }
s2 = {
    'age': 18
    }
s1.update(s2)
print(s1)
# dict.setdefault()
grazzie = s1.setdefault('age')
print(grazzie)
uncount = s1.setdefault('uncount')
print(uncount)
result = s1.setdefault('result', 2021)
print(result)

# Напишите пример вложенной функции.


def aaa(num1, num2):
    def bbb():
        print(num1 + num2)

    return bbb()


aaa(20, 12)


#Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов создающую запись в файле

funkciya = ['funkciya ', 'prinimaushaya ', 'massiv.']


def dannye(slova):
    file = open('slova.txt', 'w')
    for slovo in slova:
        file.write(slovo)
    file.close()


dannye(funkciya)

# Напишите функцию принимающую массив (состоящий из слов, название файла) и при помощи данных аргументов дополняющую файл новыми записями


massiv = ['dopolnyushaya ', 'fail ', 'novymi ', 'zapisyami.']


def dannie(words):
    file = open('words.txt', 'a')
    for word in words:
        file.write(str(word) + '\n')
    file.close()

dannie(massiv)
# Напишите функцию считывающую данные из файла
def read_to_fail():
    file = open('words.txt', 'r')
    print(file.readlines())
    file.close()

read_to_fail()
# Напишите функцию записи в файл которая принимает в себя данные, отфильтровывает их и записывает только отфильтрованные данные
words_and_nums = ['I', 'am', 18.7, 'years', 'old', 'soon', 'will', 19]

def filter(nums):
    file = open('nums.txt', 'a')
    for num in nums:
        if type(num) != str:
            file.write(str(num) + '\n')
    file.close()
filter(words_and_nums)
